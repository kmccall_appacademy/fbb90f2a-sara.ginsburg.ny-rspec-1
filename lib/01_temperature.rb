def ftoc (f_degrees)
  ((f_degrees.to_f - 32) * 5 / 9).to_i
end

def ctof (c_degrees)
  ((c_degrees.to_f * 9 / 5 + 32) * 10).to_i / 10.to_f
end
