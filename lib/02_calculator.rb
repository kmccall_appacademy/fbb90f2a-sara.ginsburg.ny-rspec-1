def add(m, n)
  m + n
end

def subtract(m, n)
  m - n
end

def sum(arr)
  sum = 0
  arr.each {|el| sum += el}
  sum
end

def multiply(*numbers)
  numbers.reduce(:*)
end

def power(b,p)
  b**p
end

def factorial(n)
  if n == 0 || n == 1
    1
  else
    (2..n).reduce(:*)
  end
end
