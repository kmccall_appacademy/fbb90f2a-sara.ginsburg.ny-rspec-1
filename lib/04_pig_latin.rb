def translate(sentence)
  translated = []
  sentence.split(" ").each do |word|
    translated << single_translation(word)
  end
  translated.join(" ")
end

def single_translation(word)
  vowels = "aeiou".split("")
  until vowels.include?word[0]
    word = move_consonant(word)
  end
  word + "ay"
end

def move_consonant(str)
  if str[0] == "q" && str[1] == "u"
    str[2...str.length] + str[0..1]
  else
    str[1...str.length] + str[0]
  end

end


if __FILE__ == $PROGRAM_NAME
  translate("square")
end
