def echo(str)
  str
end

def shout(str)
  str.upcase
end

def repeat(str, n = 2)
  output = ""
  n.times do
    output += "#{str} "
  end
  output.strip
end

def start_of_word(str, n)
  str[0...n]
end

def first_word(str)
  str.split(" ")[0]
end

def titleize(str)
  little_words = ["the","in", "at", "and", "to", "over" , "for"]
  out_arr = str.split(" ").map.each_with_index do |word,i|
    little_words.include?(word) && i > 0 ? word : word.capitalize!
  end
  out_arr.join(" ")
end
